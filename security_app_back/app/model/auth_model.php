<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Auth,
    App\Lib\Cifrado;

class AuthModel
{
    private $db;
    private $table = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function autenticar($telefono, $password) {
        $password = Cifrado::Sha512($password);
        $persona = $this->db->from($this->table)
                             ->where('Telefono', $telefono)
                             ->where('Password', $password)
                             ->where('Activo = 1')
                             ->fetch();

        if(is_object($persona)){
            //$nombre = explode(' ', $persona->Nombre)[0];
            $nombreCompleto = $persona->Nombre.' '.$persona->Apellidos;
            $token = Auth::SignIn([
                'idPersona' => $persona->idPersona,
                'Nombre' => $persona->Nombre,
                'NombreCompleto' => $nombreCompleto,
                'Email' => $persona->Email,
                'idTipoUsuario' => $persona->idTipoUsuario,
                'ImagenUsuario' => $persona->ImagenUsuario,
                'CodigoPais' => $persona->CodigoPais
            ]);

            $this->response->result = $token;

            return $this->response->SetResponse(true);
        }else{
            return $this->response->SetResponse(false, "Credenciales no válidas");
        }
    }
    public function getData($token){
        $data = Auth::GetData($token);
        if ($data === "Signature verification failed" || $data == 'Expired token' ) {
                   $this->response->errors[]='Token incorrecto';
            return $this->response->SetResponse(false);
        }else{
                   $this->response->result=$data;
            return $this->response->SetResponse(true);
        }
    }
}
