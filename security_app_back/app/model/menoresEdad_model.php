<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class MenoresEdadModel
{
    private $db;
    private $table = 'menoresEdad';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listar()
    {
        $data = $this->db->from($this->table)
                         ->fetchAll();//listar todos
       
                $this->response->result= $data;
		 return $this->response->SetResponse(true);
    }
}