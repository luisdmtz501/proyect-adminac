<?php
namespace App\Model;

use App\Lib\Response;

class ImgModel
{
	private $db;
   #	private $table;
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }
    #lsitar imagenes
    public function listar($l, $p , $user )
    {
        $data = $this->db->from($this->table)
                         ->where('IdSolicitud',$user)
                         ->limit($l)
                         ->offset($p)
                         ->orderBy('Id DESC')
                         ->fetchAll();

        $total = $this->db->from($this->table)
                          ->select('COUNT(*) Total')
                          ->fetch()
                          ->Total;

        $this->response->result = [
            'data'  => $data,
            'total' => $total
        ];
        return $this->response->SetResponse(true);
    }
     #obtener imagenes
    public function obtener($id)
    {

        $buscar =  $this->db->from($this->table,$id)
                    ->fetch();

        if ($buscar != false) {
            $this->response->result = $buscar;
            return $this->response->SetResponse(true);
         }else{
            $this->response->errors[]='La direccion no se encuentra';
            return $this->response->SetResponse(false);
         }

    }
    #cargar
    public function cargar($file,$a,$id)
    {
		
    }

}	