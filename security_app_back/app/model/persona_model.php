<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\Cifrado;

class PersonaModel
{
    private $db;
    private $table = 'persona';
    private $response;

    public function __CONSTRUCT($db)
    {
        $this->db = $db;
        $this->response = new Response();
    }

    public function listartodos()
    {
        $data = $this->db->from($this->table)
                         // ->limit($l)
                         // ->offset($p)
                         // ->orderBy('id DESC')
                         ->fetchAll();//para mas de un registro

        $this->response->result =  $data;
        return $this->response->SetResponse(true);
        // $total = $this->db->from($this->table)
        //                   ->select('COUNT(*) Total')
        //                   ->fetch()
        //                   ->Total;

        // return [
        //     'data'  => $data,
        //     'total' => $total
        // ];
    }
    
    public function obtener($id)
    {
      return $this->db->from($this->table)
                    -> where('idPersona', $id)
                    ->fetch();//para un solo dato o linea

    }

    public function registrar($data)
    {
        if (isset($data['Password'])) {
          $data['Password'] = Cifrado::Sha512($data['Password']);
        }
        $insertarPersona = $this->db->insertInto($this->table, $data)
                 ->execute();
               $this->response->result =  $insertarPersona;
        return $this->response->SetResponse(true);
    }

    public function actualizar($data,$id)
    {

      if (isset($data['Password'])) {
        $data['Password'] = Cifrado::Sha512($data['Password']);
      }

      // $img_name = basename($data['Foto']['name']);
      // $tmp_name = $data['Foto']['tmp_name'];
      // $carpeta_destino = "C:/AppServ/www/sansarita/Backend/img/";         
     
      // $t = $data['Foto']['tmp_name'];      
      try{
        // move_uploaded_file($tmp_name,$carpeta_destino.$img_name);      
        // $data['Foto'] = $carpeta_destino.$img_name;
        //$data['Foto'] = 
        $this->db->update($this->table, $data)
                 ->where('idPersona',$id)
                 ->execute();
               $this->response->result=$carpeta_destino.$img_name;
        return $this->response->SetResponse(true);
      }
      catch (Exception $e){           
        $this->response->result=$e;
        return $this->response->SetResponse(false);
      }
    }

    public function eliminar($id)
    {
        //$data['Password'] = Cifrado::Sha512($data['Password']);

        $this->db->deleteFrom($this->table,$id)
                 ->execute();

        return $this->response->SetResponse(true);
    }
}
