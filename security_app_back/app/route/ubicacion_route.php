<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\UbicacionValidation,
    App\Middleware\AuthMiddleware;

$app->group('/ubicacion/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->ubicacion->listar())
                   );
    });
});//->add(new AuthMiddleware($app));