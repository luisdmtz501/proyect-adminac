<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\personaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/persona/', function () {
    $this->get('listartodos', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->persona->listartodos())
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->obtener($args['id']))
                 );
    });

    $this->post('registrar', function ($req, $res, $args) {
      // $r = PersonaValidation::validate($req->getParsedBody());

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->registrar($req->getParsedBody()))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      // $r = PersonaValidation::validate($req->getParsedBody(),true);

      // if(!$r->response){
      //     return $res->withHeader('Content-type', 'application/json')
      //                ->withStatus(422)
      //                ->write(json_encode($r->errors));
      // }

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->persona->eliminar($args['id']))
                 );
    });
});#->add(new AuthMiddleware($app));
