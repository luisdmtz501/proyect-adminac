<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\GeneralValidation,
    App\Middleware\AuthMiddleware;

$app->group('/general/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->general->listar())
                   );
    });
});//->add(new AuthMiddleware($app));