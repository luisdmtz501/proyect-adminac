<?php
use App\Lib\login,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/img/', function () {
    $this->get('listar/{l}/{p}/{u}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->img->listar($args['l'],$args['p'],$args['u']))
                   );
    });

    $this->get('obtener/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->obtener($args['id']))
                 );
    });

    $this->post('cargar/{a}/{id}', function ($req, $res, $args) {
          $file = $req->getParsedBody();
          $a = $args['a'];
          $id = $args['id'];
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->cargar($file,$a,$id))
                 );
    });

    $this->put('actualizar/{id}', function ($req, $res, $args) {

      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->actualizar($req->getParsedBody(), $args['id']))
                 );
    });

    $this->delete('eliminar/{id}', function ($req, $res, $args) {
      return $res->withHeader('Content-type','application/json')
                 ->write(
                   json_encode($this->model->img->eliminar($args['id']))
                 );
    });
});