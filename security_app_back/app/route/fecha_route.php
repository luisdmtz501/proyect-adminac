<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\FechaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/fecha/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->fecha->listar())
                   );
    });
});//->add(new AuthMiddleware($app));