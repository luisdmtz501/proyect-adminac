<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\MujeresValidation,
    App\Middleware\AuthMiddleware;

$app->group('/mujeres/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->mujeres->listar())
                   );
    });
});//->add(new AuthMiddleware($app));