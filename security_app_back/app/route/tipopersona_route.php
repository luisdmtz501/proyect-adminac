<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\personaValidation,
    App\Middleware\AuthMiddleware;

$app->group('/tipopersona/', function () {

    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->tipopersona->listar())
                  );
    });

});
