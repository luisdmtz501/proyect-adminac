<?php
use App\Lib\Auth,
    App\Lib\Response,
    App\Validation\MenoresEdadValidation,
    App\Middleware\AuthMiddleware;

$app->group('/menoresEdad/', function () {
    $this->get('listar', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->menoresEdad->listar())
                   );
    });
});//->add(new AuthMiddleware($app));