<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Personal extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');

    // Model Respectivo
    $this->load->model('personalmodel','personal');
    $this->load->model('tipopersonamodel','tipo');
    $this->load->model('imgmodel','img');

    // $this->token = RestApi::getToken();
  }

  public function administrador(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->personal->listar(1);
      $data = $result->data;
    }catch(Exception $e){
    }
    if(isset($data)){
      $this->load->view('personal/index',
        [
          'model'=>$data
        ]
      );
    }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }

  public function crud($id = 0){
    $data = null;
    $cargos = $this->tipo->listar();
    $areas = $this->area->listar();

    if ($id > 0) {
      $data = $this->personal->obtener($id);      
    }    
    $this->load->view('header',$this->user);
    $this->load->view('personal/crud',
      [
        'model'=>$data,
        'cargos'=> $cargos->result,
        'areas'=> $areas->result
      ]
    );
    $this->load->view('footer',
        [
            'footer' => ""
        ]
    );

  }

  public function eliminar($id){
    
  }
  
}
 ?>
