<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Mujeres extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('mujeresmodel','mujeres');
  }

  public function index(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->mujeres->listar();
      //$data = $result->data;
     // var_dump($result);
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('mujeres/index',
        [
          'model'=>$result
        ]
      );
    // }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }
}