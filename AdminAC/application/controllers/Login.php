<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	 public function __CONSTRUCT(){
        parent::__construct();

        $this->load->model('authmodel','login');
    }

	public function index()
	{
		// $this->load->view('header');
		$this->load->view('login/index');
		// $this->load->view('footer');
	}

	public function autenticar()
	{
		$r = $this->login->autenticar(
			$this->input->post('Telefono'),
			$this->input->post('Password')
		);
		// $r.="}";
		
		// $r = json_decode($r); pafuncion server
		if ($r->response) {
			//seteamos token
			RestApi::setToken($r->result);
			$user = RestApi::getUserData();
			if($user->idTipoUsuario == 1){
				//lugar donde ira si es correcto
				redirect(inicio);
			}else{
				RestApi::destroyToken();
				// $this->load->view('header');
				$this->load->view('login/index',
					['error'=> 'No tiene permisos de Administrador']
				);
				// $this->load->view('footer');

			}
			//redirect();
		}else{
			// $this->load->view('header');
			$this->load->view('login/index',
				['error'=> $r->message]
			);
			// $this->load->view('footer');
		}
	}

	public function logout(){
		RestApi::destroyToken();
		redirect('');
	}
}
