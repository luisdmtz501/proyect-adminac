<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    private $user;
    
    public function __CONSTRUCT(){
        parent::__construct();
        
        $this->user = ['user' => RestApi::getUserData()];
        
        // Valida que exista el usuario obtenido del token, del caso contrario lo regresa a la pagina de inicio que es nuestro controlador auth
        if($this->user['user'] === null) redirect('');
    }
    
	public function index(){
		$this->load->view('header', $this->user);
        $this->load->view('inicio/index');
        $this->load->view('footer',
        [
            'footer' => "Inicio"
        ]);
	}
    // public function cotizar($id){
    //    // $data = null;
    //     if($id > 0)
    //     {
    //         $this->load->view('header',$this->user);
    //         $this->load->view('solicitud/cotizacion/cotizar',
    //         [
    //             'id' => $id
    //         ]);
    //         $this->load->view('footer',
    //             [
    //                 'active' => 1
    //             ]
    //         );
    //     }
    // }
    // public function mapa($id){
    //     if($id > 0)
    //     {
    //         $this->load->view('header',$this->user);
    //         $this->load->view('solicitud/cotizacion/cotizar',
    //         [
    //             'id' => $id
    //         ]);
    //         $this->load->view('footer',
    //             [
    //                 'active' => 1
    //             ]
    //         );
    //     }
    //     // $this->load->view('header',$this->user);
    //     // $this->load->view('solicitud/cotizacion/maps');
    //     // $this->load->view('footer',
    //     //     [
    //     //         'active' => 1
    //     //     ]
    //     // );        
    // }
    
    // public function guardar(){}    
    
    // public function eliminar($id){}
}