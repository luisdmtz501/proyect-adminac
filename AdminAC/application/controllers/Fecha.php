<?php
defined('BASEPATH')OR exit('No direct script access allowed');
class Fecha extends CI_Controller{
  private $user;
  // private $token;

  function __CONSTRUCT(){
    parent::__construct();

    $this->user = ['user' => RestApi::getUserData()];
    if($this->user['user'] === null)redirect('');
    $this->load->model('fechamodel','fecha');
  }

  public function index(){
    $this->load->view('header',$this->user);
    try{
      $result = $this->fecha->listar();
      //$data = $result->data;
     // var_dump($result);
    }catch(Exception $e){
      // var_dump($e);
    }
    $this->load->view('fecha/index',
        [
          'model'=>$result
        ]
      );
    // }
    $this->load->view('footer',
    [
        'footer'=>"lista"
    ]);
  }
}