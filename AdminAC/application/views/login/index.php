<!DOCTYPE html>
<html lang="es">

<head>

    
    <title>AlertaCiudadana</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Shortcut Icon" type="image/x-icon" href="<?php echo base_url('assets/icons/book.ico');?>" />
    <script src="js/sweet-alert.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('css/sweet-alert.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/material-design-iconic-font.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/normalize.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/jquery.mCustomScrollbar.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/style.css');?>">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/main.js"></script>


   
    <!--<link rel="stylesheet"  href="<?php echo base_url('css/bootstrap.min.css'); ?>">-->
    <!-- <link href="<?php echo base_url('css/bootstrap.min.css');?>" rel="stylesheet"> -->
    <!--<link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

    <link href="<?php echo base_url('css/animate.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet"> -->

</head>

<body>
    <div class="login-container full-cover-background">
      <div class="form-container">
          <p class="text-center" style="margin-top: 17px;">
             <i class="zmdi zmdi-account-circle zmdi-hc-5x"></i>
         </p>
         <h4 class="text-center all-tittles" style="margin-bottom: 30px;">inicia sesión con tu cuenta</h4>
         <?php echo form_open('login/autenticar'); ?>
         <form action="index.html">
              <div class="group-material-login">
                <input type="text" class="material-login-control" required="" maxlength="70" name="Telefono">
                <span class="highlight-login"></span>
                <span class="bar-login"></span>
                <label><i class="zmdi zmdi-account"></i> &nbsp; Telefono</label>
              </div><br>
              <div class="group-material-login">
                <input type="password" class="material-login-control" required="" maxlength="70" name="Password">
                <span class="highlight-login"></span>
                <span class="bar-login"></span>
                <label><i class="zmdi zmdi-lock"></i> &nbsp; Contraseña</label>
              </div>
              <div class="group-material">
                  <select class="material-control-login">
                      <option value="" disabled="" selected="">Tipo de usuario</option>
                      <option value="Student">Estudiante</option>
                      <option value="Teacher">Docente</option>
                      <option value="Personal">Personal administrativo</option>
                      <option value="Admin">Administrador</option>
                  </select>
              </div>
              <button class="btn-login" type="submit">Ingresar al sistema &nbsp; <i class="zmdi zmdi-arrow-right"></i></button>
              <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger">
                                '.$error.'
                            </div>';
                    }
                ?>
          </form>
      </div>   
    </div>
    <?php form_close(); ?>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url('js/jquery-1.11.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
  </body>
  </html>