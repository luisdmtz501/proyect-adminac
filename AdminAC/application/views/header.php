<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.7
*
-->
<!-- <?php var_dump($user); ?>> -->
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>AlertaCiudadana</title>
    <link rel="Shortcut Icon" type="image/x-icon" href="<?php echo base_url('assets/icons/book.ico'); ?>" />

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url('css/bootstrap.min-san.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url('css/plugins/toastr/toastr.min.css'); ?>" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url('js/plugins/gritter/jquery.gritter.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') ?>">
    <link href="<?php echo base_url('css/animate.css" rel="stylesheet'); ?>">
    <link href="<?php echo base_url('css/style-san.css" rel="stylesheet'); ?>">    
    <link href="<?php echo base_url('css/plugins/dataTables/datatables.min.css');?>" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    
    <script>
        let selectMenu = (activo) =>{
            let menu = activo.split(',')
            sessionStorage.setItem('menu',JSON.stringify(menu))
            // console.log(menu);
            
        }

        let activeMenu = () =>{
            elementos = sessionStorage.getItem(`menu`)
            console.log(elementos);
            if (elementos != null ) {
                elementos = JSON.parse(elementos)
                elementos.forEach(element => {
                    console.log(element);
                    let lista = document.getElementById(element);
                    lista.classList.add("active");
                });
            }
            
        }

    </script>

</head>
<body onload="activeMenu()">
    <div id="wrapper">
       



        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="navbar-user-top full-reset">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <img alt="image" width="50px" class="img-circle" src="<?php echo $user->ImagenUsuario ?>" />
                             </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">
                            <h4><?php echo $user->NombreCompleto;?></h4> </strong>
                             </span> <span class="text-muted text-xs block">Administrador <b class="caret"></b></span> </span> </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="profile.html">Perfil</a></li>
                                <li><a href="contacts.html">Contactos</a></li>
                                <li><a href="mailbox.html">Mensajes</a></li>
                                <li class="divider"></li>
                                <li><a onclick="return confirm('Esta seguro de querer cerrar sesion')" href="<?php echo site_url('Login/logout'); ?>">Cerrar Sesion</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            <img src="<?php echo base_url('img/user-sesion.png'); ?>" width="40">
                        </div>
                    </li>
                 <li id="Home">
                        <a onClick = "selectMenu('Home')" href="<?php echo site_url('Inicio/index/'); ?>"><h4><i class="fa fa-home"></i> <span class="nav-label">Home</span></h4></a>
                 </li>

                    <li>
                      <li id="AdministracionMenu">
                       <a href="#"><h4><i class="fa fa-cog"></i> <span class="nav-label">Consultas</span><span class="fa arrow"></h4></span></a>
                       <ul class="nav nav-second-level collapse">
                        
                    <li id="CMenu">
                        <a onClick ="selectMenu('AdministracionMenu,CMenu')" href="<?= site_url('Mujeres/index')?>"><h4><i class="fa fa-address-book-o"></i> <span class="nav-label">Mujeres</span></h4></a>
                    </li>
                    <li id="VMenu">
                         <a onClick ="selectMenu('AdministracionMenu,VMenu')" href="<?= site_url('MenoresEdad/index')?>"><h4><i class="fa fa-address-book-o"></i> <span class="nav-label">Menores de Edad</span></h4></a>
                    </li>
                    <li id="GMenu">
                        <a onClick ="selectMenu('AdministracionMenu,GMenu')" href="<?= site_url('Ubicacion/index')?>"><h4><i class="fa fa-barcode"></i> <span class="nav-label">Ubicacion</span></h4></a>
                    </li>
                    <li id="ClMenu">
                        <a onClick ="selectMenu('AdministracionMenu,ClMenu')" href="<?= site_url('Fecha/index'); ?>"><h4><i class="fa fa-files-o"></i> <span class="nav-label">Fecha</span></h4></a>
                    </li>
                    <li id="pMenu">
                        <a onClick ="selectMenu('AdministracionMenu,pMenu')" href="<?= site_url('General/index')?>"><h4><i class="fa fa-th"></i> <span class="nav-label">General</span></h4></a>
                    </li>
                   </ul>
                  </li>
                 </li>
            

                 <li id="Estadistica">
                        <a onClick = "selectMenu('Estadistica')" href="#"><h4><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Estadistica</span></h4></a>
                 </li>
                        </ul>
                    </li>
                 </li>                   
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar bg-danger navbar-static-top" role="navigation" style="margin-bottom: 0">
        
        <div class="navbar-header">
            <a class="" href="#"><i class=""></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                </div>
            </form>
        </div>
        
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <h3><span class="m-r-sm text-white welcome-message"><strong>SISTEMA DE ADMINISTRACION DE ALERTA CIUDADANA</strong></h3></span>
                </li>
                
                <li class="bg-white">
                    <a onclick="return confirm('Esta seguro de querer cerrar sesion')" href="<?php echo site_url('Login/logout'); ?>">
                        <i class="fa fa-sign-out text-white"></i><span class="text-white">Cerrar Sesion</span>
                    </a>
                </li>
                <li>
                   <!--  <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a> -->
                </li>
            </ul>
      </nav>
    </div>
  </body>
</html>