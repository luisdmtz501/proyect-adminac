<!-- 
*
-->
<?php 
  //   var_dump($model);
?>
<!DOCTYPE html>
<html lang="es">
<div class="row wrapper border-bottom white-bg page-heading">
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Alerta Ciudadana - Mujeres <!-- <small>texto</small> --></h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="statistics">
                    <div class="container-fluid"  style="margin: 10px 0;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 col-md-3">
                            <img src="<?php echo base_url('img/mujer1.bmp'); ?>" alt="chart" class="img-responsive center-box" style="max-width: 80px;">
                            </div>
                            <div class="col-xs-12 col-sm-3 col-md-8 text-justify lead">
                                Aquí puedes consultar la información de solicitudes de "ALERTA" con respecto a Fechas.
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12">
                                <h3 class="text-center all-tittles">Total de Alertas</h3>
                                <div class="table-responsive">
                                    <table class="table table-hover text-center">
                                        <thead>
                                            <tr class="success">
                                                <th class="text-center">Edad</th>
                                                <th class="text-center">Municipio</th>
                                                <th class="text-center">Hora</th>
                                                <!-- <th class="text-center">Estado</th>  -->
                                            </tr>
                                        </thead>
                                        <!

                                        <tbody>
                                        <?php foreach ($model->result as $m):?>
                                            <tr>
                                                <td><?php echo $m->Edad; ?></td>
                                                <td><?php echo $m->Municipio; ?></td>
                                                <td><?php echo $m->Hora; ?></td>
                                            </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    
                                        <!-- <tfoot>
                                            <tr class="info">
                                                <th class="text-center">Total</th>
                                                <th class="text-center">0</th>
                                                <th class="text-center">0%</th>
                                            </tr>
                                        </tfoot>  -->
                                    </table>
                                </div>
                                <p class="lead text-center"><strong><i class="zmdi zmdi-info-outline"></i>&nbsp; ¡Importante!</strong> Los datos mostrados son los existentes al día de hoy”</p>
                            </div>
                        </div>
                        
                    
                        <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Acerca de</h4>
                        <p>
                            Alerta Ciudadana es un programa que brinda apoyo en la atención de situaciones de riesgo.
                        </p>
                    </div>
    </div>
</div>
</html>