
    </div>
    </div>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url('js/jquery-3.1.1.min.js');?>"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>

    <!-- Flot -->
    <script src="<?php echo base_url('js/plugins/flot/jquery.flot.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/flot/jquery.flot.tooltip.min.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/flot/jquery.flot.spline.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/flot/jquery.flot.resize.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/flot/jquery.flot.pie.js');?>"></script>

    <!-- Peity -->
    <script src="<?php echo base_url('js/plugins/peity/jquery.peity.min.js');?>"></script>
    <script src="<?php echo base_url('js/demo/peity-demo.js');?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url('js/inspinia.js');?>"></script>
    <script src="<?php echo base_url('js/plugins/pace/pace.min.js');?>"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url('js/plugins/jquery-ui/jquery-ui.min.js');?>"></script>

    <!-- GITTER -->
    <script src="<?php echo base_url('js/plugins/gritter/jquery.gritter.min.js');?>"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url('js/plugins/sparkline/jquery.sparkline.min.js');?>"></script>

    <!-- Sparkline demo data  -->
    <script src="<?php echo base_url('js/demo/sparkline-demo.js');?>"></script>

    <!-- ChartJS-->
    <script src="<?php echo base_url('js/plugins/chartJs/Chart.min.js');?>"></script>

    <!-- Toastr -->
    <script src="<?php echo base_url('js/plugins/toastr/toastr.min.js');?>"></script>
    
    <?php if($footer == 'lista'): ?>
    <!-- Page-Level Scripts -->
    <script src="<?php echo base_url('js/plugins/dataTables/datatables.min.js');?>"></script>

        <script>
            $(document).ready(function(){
                $('.dataTables-example').DataTable({
                    pageLength: 25,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel', title: 'AlertaCiudadaba'},
                        {extend: 'pdf', title: 'AlertaCiudadana'},

                        {extend: 'print',
                         customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]

                });

            });

        </script>
    <?php endif; ?>
</body>
</html>
