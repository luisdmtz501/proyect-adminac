 <?php
 class TipoPersonaModel extends CI_Model{

    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "tipopersona/listar"
        );
    }
}