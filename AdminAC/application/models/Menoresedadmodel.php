<?php
 class MenoresEdadModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "menoresEdad/listar"
        );
    }
}