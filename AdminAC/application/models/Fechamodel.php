<?php
 class FechaModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "fecha/listar"
        );
    }
}