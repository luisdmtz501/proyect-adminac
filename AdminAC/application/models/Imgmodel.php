<?php
 class ImgModel extends CI_Model{
    public function cargar($a,$id,$file){
        return RestApi::call(
            RestApiMethod::POST,
            "img/cargar/$a/$id",
            $file
        );
    }
}