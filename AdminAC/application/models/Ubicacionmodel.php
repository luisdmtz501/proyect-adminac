<?php
 class  UbicacionModel extends CI_Model{
    public function listar(){
        return RestApi::call(
            RestApiMethod::GET,
            "ubicacion/listar"
        );
    }
}