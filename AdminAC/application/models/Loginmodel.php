 <?php
 class LoginModel extends CI_Model{
    public function autenticar($email,$password){
        return RestApi::call(
            RestApiMethod::POST,
            "auth/autenticar",
            [
            	"Email" => $email,
            	"Password" => $password
                //"TipoPersona" => 'Admin'
            ]
        );
    }
}