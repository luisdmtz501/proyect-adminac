 <?php
 class AuthModel extends CI_Model{
    public function autenticar($telefono,$password){
        return RestApi::call(
            RestApiMethod::POST,
            "auth/autenticar",
            [
            	"Telefono" => $telefono,
            	"Password" => $password
                //"TipoPersona" => 'Admin'
            ]
        );
    }
}